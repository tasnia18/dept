from django.forms import ModelForm
from .models import Dept
from django import forms


class DeptForm(ModelForm):
    class Meta:
        model= Dept
        fields = '__all__'


class DEPTform(forms.Form):
    name=forms.CharField(max_length=200)
    HoD=forms.CharField(max_length=200)
    total_student=forms.IntegerField()
    total_credit=forms.FloatField()