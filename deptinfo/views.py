from django.shortcuts import render, redirect
from .models import Dept,DeptDetails
from .forms import DeptForm,DEPTform
from django.http import HttpResponseRedirect
from django.views import View

def dept(request): #display
    d = Dept.objects.all()
    return render(request,'dept/home.html',{'info':d,'f':'by function'})

def form(request): #insert
    form = DeptForm()
    if request.method=='POST':
        form = DeptForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/dept/')
        
    return render(request,'dept/form.html',{'form':form,'f':'by function'})
def updateinfo(request,pk):  #update
    update= Dept.objects.get(id=pk)
    if request.method=='POST':
        form = DeptForm(request.POST,instance=update)
        if form.is_valid:
            form.save()
            return redirect('/dept/')

    form = DeptForm(instance=update)
    return render(request,'dept/form.html',{'form':form})

def deleteinfo(request,pk): #delete
    dlt= Dept.objects.get(id=pk)
    dlt.delete()
    return redirect('/dept/')

class Dept1(View): #display
    def get(self, request, *args, **kwargs):
        d = Dept.objects.all()
        return render(request,'dept/home1.html',{'info':d,'c':'by class'})

class Form1(View): #insert

    def get(self, request, *args, **kwargs):
        form = DeptForm()
        context = {'form':form,'c':'by class'}
        return render(request,'dept/form1.html',context)

    def post(self, request, *args, **kwargs):
        form = DeptForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/dept1/')


class Update_info(View):
    def get(self, request, pk, *args, **kwargs):
        update= Dept.objects.get(id=pk)
        form = DeptForm(instance=update)
        context = {'form':form}
        return render(request,'dept/form1.html',context)

    def post(self, request,pk, *args, **kwargs):
        update= Dept.objects.get(id=pk)
        form = DeptForm(request.POST,instance=update)
        if form.is_valid:
            form.save()
            return redirect('/dept1/')

class Delete_info(View):
   

    def get(self, request, pk, *args, **kwargs):
        dlt= Dept.objects.get(id=pk)
        dlt.delete()
        d = Dept.objects.all()
        return render(request,'dept/home1.html',{'info':d})



def dept2(request): #display
    d = DeptDetails.objects.all()
    return render(request,'dept/customhome.html',{'info':d})


def customform(request):
    if request.method=='POST':
        form = DEPTform(request.POST)
        if form.is_valid():
            name= form.cleaned_data['name']
            HoD= form.cleaned_data['HoD']
            total_student= form.cleaned_data['total_student']
            total_credit= form.cleaned_data['total_credit']
            dept_obj = DeptDetails(name=name,HoD=HoD,total_student=total_student,total_credit=total_credit)
            dept_obj.save()
            return redirect('/dept2/')
    else:
        form = DEPTform()

    return render(request,'dept/customform.html',{'form':form})


def c_update(request,pk):
    
    if request.method=='POST':
        form = DEPTform(request.POST)
        if form.is_valid():
            name= form.cleaned_data['name']
            HoD= form.cleaned_data['HoD']
            total_student= form.cleaned_data['total_student']
            total_credit= form.cleaned_data['total_credit']
            dept_obj = DeptDetails(id=pk,name=name,HoD=HoD,total_student=total_student,total_credit=total_credit)
            dept_obj.save()
            return redirect('/dept2/')
    else:
        dp= DeptDetails.objects.get(id=pk)
        form = DEPTform()

    return render(request,'dept/updateform.html',{'form':form,'dept':dp})



'''
def c_update(request,pk):  #update
    update = DeptDetails.objects.get(id=pk)
    if request.method=='POST':
        form = DEPTform(request.POST,instance=update)
        if form.is_valid:
            name= form.cleaned_data['name']
            HoD= form.cleaned_data['HoD']
            total_student= form.cleaned_data['total_student']
            total_credit= form.cleaned_data['total_credit']
            dept_obj = DeptDetails(name=name,HoD=HoD,total_student=total_student,total_credit=total_credit)
            dept_obj.save()
            return redirect('/dept2/')

    form = DEPTform(instance=update)
    return render(request,'dept/customform.html',{'form':form})

'''