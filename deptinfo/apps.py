from django.apps import AppConfig


class DeptinfoConfig(AppConfig):
    name = 'deptinfo'
