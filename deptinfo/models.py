from django.db import models

# Create your models here.
class Dept(models.Model):
    name=models.CharField(max_length=200)
    HoD=models.CharField(max_length=200)
    total_student=models.IntegerField()
    total_credit=models.FloatField()

    def __str__(self):
        return self.name

class DeptDetails(models.Model):
    name=models.CharField(max_length=200)
    HoD=models.CharField(max_length=200)
    total_student=models.IntegerField()
    total_credit=models.FloatField()

    def __str__(self):
        return self.name