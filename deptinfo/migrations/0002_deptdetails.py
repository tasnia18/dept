# Generated by Django 3.0.7 on 2020-07-07 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deptinfo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeptDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('HoD', models.CharField(max_length=200)),
                ('total_student', models.IntegerField()),
                ('total_credit', models.FloatField()),
            ],
        ),
    ]
