"""dept URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from deptinfo import views

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('dept/', views.dept,name='dept'),
    path('form/', views.form,name='form'),
    path('updateinfo/<str:pk>/', views.updateinfo,name='updateinfo'),
    path('deleteinfo/<str:pk>/', views.deleteinfo,name='deleteinfo'),

    path('dept1/', views.Dept1.as_view(),name='dept1'),
    path('form1/', views.Form1.as_view(),name='form1'),
    path('updateinfo1/<str:pk>/', views.Update_info.as_view(),name='updateinfo1'),
    path('deleteinfo1/<str:pk>/', views.Delete_info.as_view(),name='deleteinfo1'),

    path('customform/', views.customform,name='customform'),
    path('c_update/<str:pk>/', views.c_update,name='c_update'),
    path('dept2/', views.dept2,name='dept2'),

]
